from rss import RSS


# Grabber for feeds implementing the Media RSS extension (namespace
# xmlns:media).

class MediaRSS(RSS):

    def parse_entry(self, entry):
        episode = RSS.parse_entry(self, entry)

        if entry.has_key("media_content") \
                and len(entry.media_content) > 0:
            episode.url = entry.media_content[0].get("url")
            episode.mime_type = entry.media_content[0].get("type")

        if entry.has_key("media_thumbnail") \
                and len(entry.media_thumbnail) > 0:
            episode.thumbnail_url = entry.media_thumbnail[0].get("url")

        return episode
