from mediarss import MediaRSS
import time

# Grabber for the Meet the Gimp feed.

class MeetTheGimp(MediaRSS):

    published = None

    def __init__(self, url):
        MediaRSS.__init__(self, url)
        self.published = datetime.now()

    def parse_entry(self, entry):
        episode = MediaRSS.parse_entry(self, entry)

        if episode.published_parsed is None:
            pass

        if episode.has_key("published_parsed") and episode.published_parsed != None:
            published = datetime.fromtimestamp(time.mktime(episode.published_parsed))

        return episode
