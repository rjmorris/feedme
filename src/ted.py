from mediarss import MediaRSS
import re


# Grabber for the TED feed. The titles for all the episodes in the feed are
# formatted as:
#
#   TED: Speaker Name: Title of presentation - Speaker Name (Year)
#
# or (if there are multiple speakers, for example):
#
#   TED: Title of presentation - Speaker Name (Year)
#
# This grabber removes TED and the Speaker Name from the beginning of the title,
# changing it to:
#
#   Title of presentation - Speaker Name (Year)

class TED(MediaRSS):

    def parse_entry(self, entry):
        episode = MediaRSS.parse_entry(self, entry)

        episode.title = re.sub("^TED: ", "", episode.title)
        episode.title = re.sub("^.*?: ", "", episode.title)

        return episode
