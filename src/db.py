from sqlalchemy import create_engine
from sqlalchemy.sql import text


class MythDB:
    def __init__(self, host, port, dbname, username, password):
        self.engine = create_engine(
            f"mysql+mysqldb://{username}:{password}@{host}:{port}/{dbname}?charset=utf8mb4",
        )

    def is_episode_recorded(self, title, subtitle, start_time):
        """
        Return True if the episode has already been saved in the MythTV database.
        """

        # If the episode matches an existing episode's title or start time,
        # assume they're the same episode. This isn't a perfect system but may
        # be the best we can do. I've attempted to write it so that the most
        # likely scenarios are handled correctly, and only the rare cases are
        # mishandled.
        #
        # The best way to tell whether an episode has been recorded would be to
        # search for a unique episode ID. However, many RSS feeds don't provide
        # such an ID, and even if they did there is a chance they would change
        # it at some point.
        #
        # An episode may match on the start time but not the title under several
        # scenarios, listed in descending order of likelihood:
        #
        #   1) It's the same episode, but the the title has been tweaked in the
        #      RSS feed. This function correctly concludes that the episode has
        #      been previously recorded.
        #
        #   2) It's a different episode, but the RSS feed specifies a start time
        #      that is identical to another episode. This function incorrectly
        #      concludes that the episode has been previously recorded, so the
        #      episode is lost.
        #
        #   3) It's a different episode with a unique start time in the RSS
        #      feed, but changes or bugs in MythTV's date handling has altered
        #      the date so that it matches another episode. This function
        #      incorrectly concludes that the episode has been previously
        #      recorded, so the episode is lost.
        #
        # An epsiode may match on the title but not the start time under several
        # scenarios, listed in descending order of likelihood:
        #
        #   1) It's the same episode, but changes or bugs in MythTV's date
        #      handling have altered the start time in the database from what
        #      was stored the last time the episode was recorded. This
        #      function correctly concludes that the episode has been previously
        #      recorded.
        #
        #   2) It's the same episode, but the start time has been tweaked in the
        #      RSS feed. This function correctly concludes that the episode has
        #      been previously recorded.
        #
        #   3) It's a different episode, but the RSS feed gives it the same
        #      title as another episode. This function incorrectly concludes
        #      that the episode has been previously recorded, so the episode is
        #      lost.
        #
        # An epsiode may match on neither the title nor the start time under several
        # scenarios, listed in descending order of likelihood:
        #
        #   1) It's a different episode. This function correctly concludes that
        #      the episode hasn't been previously recorded.
        #
        #   2) It's the same episode, but the RSS feed has tweaked its title and
        #      start time. This function incorrectly concludes that the episode
        #      hasn't been previously recorded, so the episode is re-recorded.
        #
        #   3) It's the same episode, but the RSS feed has tweaked its title and
        #      changes or bugs in MythTV's date handling have altered the start
        #      time in the database from what was stored the last time the
        #      episode was recorded. This function incorrectly concludes that
        #      the episode hasn't been previously recorded, so the episode is
        #      re-recorded.
        #
        # An epsiode may match on both the title and the start time under several
        # scenarios, listed in descending order of likelihood:
        #
        #   1) It's the same episode. This function correctly concludes that the
        #      episode has been previously recorded.
        #
        #   2) It's a different episode, but the RSS feed assigned it an
        #      identical title and start time as another episode. This function
        #      incorrectly concludes that the episode has been previously
        #      recorded, so the episode is lost.
        #
        #   3) It's a different episode, but the RSS feed assigned it an
        #      identical title, and changes or bugs in MythTV's date handling
        #      has altered the date so that it matches another episode. This
        #      function incorrectly concludes that the episode has been
        #      previously recorded, so the episode is lost.

        query = text(
            "SELECT COUNT(*)"
            "  FROM oldrecorded"
            "  WHERE title=:title AND ("
            "    subtitle=:subtitle OR starttime=:starttime"
            "  )"
        )

        num_recorded = self.engine.execute(
            query,
            title=title,
            subtitle=subtitle,
            starttime=start_time.strftime("%Y-%m-%d %H:%M:%S"),
        ).scalar()

        return num_recorded > 0

    def num_episodes_unwatched(self, feed_title):
        """
        Return the number of unwatched episodes in the MythTV database for the feed
        with the given title.
        """

        query = text(
            "SELECT COUNT(*)"
            "  FROM recorded"
            "  WHERE title=:title AND watched=0"
        )

        num_unwatched = self.engine.execute(
            query,
            title=feed_title,
        ).scalar()

        return num_unwatched

    def add_episode(
            self,
            channel_id,
            station,
            start_time,
            end_time,
            title,
            subtitle,
            description,
            category,
            original_air_date,
            hostname,
            last_modified,
            file_size,
            base_name,
            stereo=False,
            audio_props="",
            hdtv=False,
            video_props="",
    ):
        """
        Add an episode to the database.
        """

        # We're assuming that these attributes will be the same for all
        # episodes, but the default value defined in the database schema isn't
        # the one we want to use, so define our own defaults. We can move these
        # into the argument list later if we decide to assign different values
        # for each episode.
        default_season = 0
        default_episode = 0
        default_inetref = ""
        default_generic = 0
        default_subtitle_types = ""
        default_category_type = "series"
        default_recstatus = -3  # -3 means "recorded"
        default_profile = "No"
        default_duplicate = 1

        query_old_recorded = create_insert_query(
            table="oldrecorded",
            chanid=channel_id,
            starttime=start_time.strftime("%Y-%m-%d %H:%M:%S"),
            endtime=end_time.strftime("%Y-%m-%d %H:%M:%S"),
            title=title,
            subtitle=subtitle,
            description=description,
            category=category,
            station=station,
            season=default_season,
            episode=default_episode,
            inetref=default_inetref,
            generic=default_generic,
            recstatus=default_recstatus,
        )
        query_recorded = create_insert_query(
            table="recorded",
            chanid=channel_id,
            starttime=start_time.strftime("%Y-%m-%d %H:%M:%S"),
            endtime=end_time.strftime("%Y-%m-%d %H:%M:%S"),
            originalairdate=original_air_date.strftime("%Y-%m-%d"),
            title=title,
            subtitle=subtitle,
            description=description,
            category=category,
            hostname=hostname,
            lastmodified=last_modified.strftime("%Y-%m-%d %H:%M:%S"),
            filesize=file_size,
            basename=base_name,
            progstart=start_time.strftime("%Y-%m-%d %H:%M:%S"),
            progend=end_time.strftime("%Y-%m-%d %H:%M:%S"),
            season=default_season,
            episode=default_episode,
            inetref=default_inetref,
            profile=default_profile,
            duplicate=default_duplicate,
        )
        query_recorded_program = create_insert_query(
            table="recordedprogram",
            chanid=channel_id,
            starttime=start_time.strftime("%Y-%m-%d %H:%M:%S"),
            endtime=end_time.strftime("%Y-%m-%d %H:%M:%S"),
            airdate=original_air_date.strftime("%Y"),
            originalairdate=original_air_date.strftime("%Y-%m-%d"),
            title=title,
            subtitle=subtitle,
            description=description,
            category=category,
            category_type=default_category_type,
            stereo=int(stereo),
            hdtv=int(hdtv),
            audioprop=audio_props,
            videoprop=video_props,
            subtitletypes=default_subtitle_types,
        )

        with self.engine.begin() as conn:
            conn.execute(query_old_recorded)
            conn.execute(query_recorded)
            conn.execute(query_recorded_program)

def create_insert_query(table, **kwargs):
    query = f"INSERT INTO {table} ("
    query += ",".join(kwargs.keys())
    query += ") VALUES ("
    query += ",".join(f":{k}" for k in kwargs.keys())
    query += ")"

    return text(query).bindparams(**kwargs)
