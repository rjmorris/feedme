from rss import RSS


# Grabber for the HD Trailers feed. It's a basic RSS feed, but the feed items
# from apple.com don't actually include a video file. They just redirect to
# trailers.apple.com. Skip those trailers.

class HDTrailers(RSS):

    def parse_entry(self, entry):
        episode = RSS.parse_entry(self, entry)

        if episode.url is not None \
                and episode.url.startswith("http://trailers.apple.com"):
            return None

        return episode
