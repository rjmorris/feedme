from feedme import Episode
from grabberbase import GrabberBase
from datetime import datetime
import time


class Hubblecast(GrabberBase):

    published = None

    def __init__(self, url):
        GrabberBase.__init__(self, url)
        self.published = datetime.now()

    def parse_entry(self, entry):
        episode = Episode()

        if entry.has_key("title"):
            episode.title = entry.title

        if entry.has_key("subtitle"):
            episode.description = entry.subtitle

        # If the entry doesn't have a published date, then use the date from the
        # previously parsed entry.
        if entry.has_key("published_parsed") and entry.published_parsed != None:
            self.published = datetime.fromtimestamp(time.mktime(entry.published_parsed))
        episode.start_date = self.published

        if entry.has_key("enclosures") \
                and len(entry.enclosures) > 0:
            episode.url = entry.enclosures[0].get("url")
            episode.mime_type = entry.enclosures[0].get("type")

        if entry.has_key("media_thumbnail") \
                and len(entry.media_thumbnail) > 0:
            episode.thumbnail_url = entry.media_thumbnail[0].get("url")

        return episode
