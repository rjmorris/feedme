import re
import os
import os.path
import sys
import subprocess
import urllib
import stat
import logging
from datetime import datetime, timedelta
from argparse import ArgumentParser
from configparser import RawConfigParser
import importlib
from lxml import etree
from PIL import Image
import socket

from db import MythDB


class Episode:

    title = None
    url = None
    mime_type = None
    path = None
    extension = None
    start_date = None
    end_date = None
    description = ""
    thumbnail_url = None
    thumbnail_path = None
    thumbnail_extension = None

    feed_title = None
    feed_category = None

    duration = 0
    video_props = ""
    audio_props = ""
    stereo = False
    hdtv = False
    width = 0
    height = 0
    aspect = 0

    def is_valid(self):
        return self.title is not None \
            and len(self.title) > 0 \
            and self.url is not None \
            and len(self.url) > 0 \
            and self.start_date is not None \
            and (self.mime_type is None or self.mime_type.startswith("video"))


class Feedme:

    logger = None
    config = None

    channel_name = "Feedme"
    channel_id = 9998
    channel_num = 9998
    videos_dir = "/data/mythtv/recordings"
    title_len = 40
    subtitle_len = 128

    required_thumbnail_extension = ".png"


    def __init__(self, config, mythdb):
        self.logger = logging.getLogger("feedme")
        self.config = config

        if self.config.has_option("feedme", "channel_name"):
            self.channel_name = self.config.get("feedme", "channel_name")
        if self.config.has_option("feedme", "channel_id"):
            self.channel_id = self.config.get("feedme", "channel_id")
        if self.config.has_option("feedme", "channel_num"):
            self.channel_num = self.config.get("feedme", "channel_num")
        if self.config.has_option("feedme", "videos_dir"):
            self.videos_dir = self.config.get("feedme", "videos_dir")
        if self.config.has_option("feedme", "title_len"):
            self.title_len = self.config.get("feedme", "title_len")
        if self.config.has_option("feedme", "subtitle_len"):
            self.subtitle_len = self.config.get("feedme", "subtitle_len")

        self.mythdb = mythdb

    def run(self, sections):
        found_section = False

        for section in sections:
            if section == "feedme" or section == "defaults":
                continue
            found_section = True

            feed_title = section

            self.set_config_value(section, "earliest_date", "1900-01-01")
            self.set_config_value(section, "download_newest", "true")
            self.set_config_value(section, "max_unwatched", "10")
            self.set_config_value(section, "download_thumbnail", "true")
            self.set_config_value(section, "category", "Feeds")

            earliest_date = datetime.strptime(self.config.get(section, "earliest_date"), "%Y-%m-%d")
            download_newest = self.config.getboolean(section, "download_newest")
            max_unwatched = self.config.getint(section, "max_unwatched")
            download_thumbnail = self.config.getboolean(section, "download_thumbnail")
            category = self.config.get(section, "category")

            self.logger.info("processing feed " + feed_title)
            self.logger.debug("  earliest date = " + earliest_date.strftime("%Y-%m-%d"))
            self.logger.debug("  download newest = " + str(download_newest))
            self.logger.debug("  max_unwatched = " + str(max_unwatched))
            self.logger.debug("  download thumbnail = " + str(download_thumbnail))
            self.logger.debug("  category = " + category)

            current_unwatched = self.mythdb.num_episodes_unwatched(
                feed_title=feed_title[:int(self.title_len)],
            )
            if max_unwatched > 0 and current_unwatched >= max_unwatched:
                self.logger.info("skipping feed because too many episodes have been recorded and are unwatched: " + str(current_unwatched))
                continue

            try:
                grabber_module = importlib.import_module(self.config.get(section, "grabber").lower())
                grabber_class = getattr(grabber_module, self.config.get(section, "grabber"))
            except:
                self.logger.exception("failed to initialize grabber " + self.config.get(section, "grabber"))
                continue
            try:
                episodes = grabber_class(self.config.get(section, "url")).get_episodes()
            except:
                self.logger.exception("failed to parse feed")
                continue

            if download_newest:
                episodes.sort(key = lambda ep: ep.start_date, reverse = True)
            else:
                episodes.sort(key = lambda ep: ep.start_date)

            for episode in episodes:
                episode.feed_title = feed_title
                episode.feed_category = category

                if self.mythdb.is_episode_recorded(
                        title=episode.feed_title[:int(self.title_len)],
                        subtitle=episode.title[:int(self.subtitle_len)],
                        start_time=episode.start_date,
                ):
                    self.logger.debug("skipping episode that has already been recorded: " + episode.title)
                    continue

                self.logger.debug("episode date = " + str(episode.start_date) + ", min date = " + str(earliest_date))
                if episode.start_date <= earliest_date:
                    self.logger.debug("skipping episode that is too old (" + str(episode.start_date) + "): " + episode.title)
                    if download_newest:
                        self.logger.debug("skipping rest of feed because all remaining episodes are too old")
                        break
                    else:
                        continue

                self.logger.info("accepting episode " + episode.title)

                try:
                    self.assign_path(episode)
                except:
                    self.logger.exception("skipping episode because an exception occurred while assigning a local path: " + episode.title)
                    continue

                try:
                    self.download_video(episode)
                except:
                    if os.path.isfile(episode.path):
                        os.remove(episode.path)
                    self.logger.exception("skipping episode because an exception occurred while downloading: " + episode.title)
                    continue

                try:
                    self.extract_video_details(episode)
                except:
                    if os.path.isfile(episode.path):
                        os.remove(episode.path)
                    self.logger.exception("skipping episode because an exception occurred while extracting video details: " + episode.title)
                    continue

                # Add the episode to the database in a separate try-block. If
                # this fails, abort the entire program, because we don't want to
                # continue if the database is potentially in a bad state.
                try:
                    self.mythdb.add_episode(
                        channel_id=self.channel_id,
                        station=self.channel_name,
                        start_time=episode.start_date,
                        end_time=episode.end_date,
                        title=episode.feed_title[:int(self.title_len)],
                        subtitle=episode.title[:int(self.subtitle_len)],
                        description=episode.description,
                        category=episode.feed_category,
                        original_air_date=episode.start_date,
                        hostname=socket.gethostname(),
                        last_modified=episode.end_date,
                        file_size=os.stat(episode.path).st_size,
                        base_name=os.path.basename(episode.path),
                        stereo=episode.stereo,
                        hdtv=episode.hdtv,
                        audio_props=episode.audio_props,
                        video_props=episode.video_props,
                    )
                except:
                    if os.path.isfile(episode.path):
                        os.remove(episode.path)
                    self.logger.exception("aborting because an exception occurred while adding episode to database: " + episode.title)
                    sys.exit(-1)

                try:
                    if download_thumbnail and episode.thumbnail_url is not None:
                        self.download_thumbnail(episode)
                    else:
                        self.generate_thumbnail(episode)
                except:
                    pass

                current_unwatched += 1
                if max_unwatched > 0 and current_unwatched >= max_unwatched:
                    self.logger.debug("skipping rest of feed because too many episodes have been recorded and are unwatched: " + str(current_unwatched))
                    break

        if not found_section:
            self.logger.warn("no feed sections found in config file, nothing to do")

    def set_config_value(self, section, option, default):
        if self.config.has_option(section, option):
            # do nothing, the section already has this option
            pass
        elif self.config.has_option("defaults", option):
            # take the option value from the defaults section
            self.config.set(section, option, self.config.get("defaults", option))
        else:
            # take the option value from the provided default
            self.config.set(section, option, default)


    def assign_path(self, episode):
        # Construct the path and filename where we will store the downloaded
        # episode. Follow the MythTV convention of naming the file based on the
        # channel ID and start time. To avoid duplicate file names, add a second
        # to the start time until the resulting file name hasn't been used
        # already.
        #
        # TODO: Query MythTV for a storage group location instead of hardcoding
        # the path.

        episode.extension = os.path.splitext(episode.url)[1]

        # Override the file extension if a known MIME type is given.
        if episode.mime_type is not None:
            type = episode.mime_type.lower()

            if type == "video/mp4":
                episode.extension = ".mp4"
            elif type == "video/x-m4v":
                episode.extension = ".m4v"
            elif type == "video/mpeg":
                episode.extension = ".mpg"
            elif type == "video/x-msvideo":
                episode.extension = ".avi"
            elif type == "video/quicktime":
                episode.extension = ".mov"
            elif type == "video/mov":
                episode.extension = ".mov"
            elif type == "video/ogg":
                episode.extension = ".ogv"
            elif type == "video/webm":
                episode.extension = ".webm"
            elif type == "video/x-matroska":
                episode.extension = ".mkv"
            elif type == "video/x-flv":
                episode.extension = ".flv"
            else:
                self.logger.warn("unknown MIME type " + type + ", taking extension from URL")

        while True:
            episode.path = os.path.join(self.videos_dir, "%s_%s%s" % (self.channel_id, episode.start_date.strftime("%Y%m%d%H%M%S"), episode.extension))
            if not os.path.isfile(episode.path):
                break
            episode.start_date += timedelta(seconds=1)


    def download_video(self, episode):    
        self.logger.info("downloading video...")
        self.logger.info("  remote URL: " + episode.url)
        self.logger.info("  local path: " + episode.path)

        urllib.request.urlretrieve(episode.url, episode.path)
        old_umask = os.umask(000)
        os.chmod(episode.path, stat.S_IWUSR | stat.S_IRUSR | stat.S_IWGRP | stat.S_IRGRP | stat.S_IWOTH | stat.S_IROTH)
        os.umask(old_umask)


    def extract_video_details(self, episode):
        self.logger.debug("querying video for details")

        duration_ms = subprocess.check_output(
            ["mediainfo", "--inform=General;%Duration%", episode.path],
            stderr = subprocess.STDOUT).rstrip()
        self.logger.debug("  duration = %s ms" % duration_ms)
        episode.duration = float(duration_ms) / 1000.0

        width = subprocess.check_output(
            ["mediainfo", "--inform=Video;%Width%", episode.path],
            stderr = subprocess.STDOUT).rstrip()
        self.logger.debug("  width = %s px" % width)
        episode.width = int(width)

        height = subprocess.check_output(
            ["mediainfo", "--inform=Video;%Height%", episode.path],
            stderr = subprocess.STDOUT).rstrip()
        self.logger.debug("  height = %s px" % height)
        episode.height = int(height)

        channels = subprocess.check_output(
            ["mediainfo", "--inform=Audio;%Channel(s)%", episode.path],
            stderr = subprocess.STDOUT).rstrip()
        self.logger.debug("  audio channels = %s" % channels)
        if channels == "1":
            episode.audio_props = "MONO"
        elif channels == "2":
            episode.audio_props = "STEREO"
            episode.stereo = 1
        elif channels == "6":
            episode.audio_props = "SURROUND"

        episode.end_date = episode.start_date + timedelta(seconds = episode.duration)

        episode.aspect = float(episode.width) / float(episode.height)

        if episode.height >= 720:
            episode.hdtv = 1

        if episode.width == 1920 and episode.height == 1080:
            episode.video_props = "HDTV,1080"
        elif episode.width == 1280 and episode.height == 720:
            episode.video_props = "HDTV,720"
        elif episode.aspect >= 1.77:
            episode.video_props = "WIDESCREEN"


    def generate_thumbnail(self, episode):
        # Create the thumbnail. This must happen after the records are added to
        # the database to prevent mythtv from thinking the thumbnail is old and
        # regenerating it (which it doesn't do properly for some reason).
        self.logger.debug("taking screenshot for thumbnail")

        episode.thumbnail_extension = self.required_thumbnail_extension
        episode.thumbnail_path = episode.path + episode.thumbnail_extension

        if episode.duration < 120:
            delay = int(episode.duration / 2)
        else:
            delay = 60

        # Ideally we would use mythpreviewgen to create the thumbnail. However,
        # it doesn't work properly: it generates a garbled or blank image. I
        # suspect this is because there is no seek table. Running mythtranscode
        # will build a seek table, which in some cases has allowed
        # mythpreviewgen to run correctly, but the presence of the seek table
        # has messed up the skip forward/back navigation during playback. This
        # is a much bigger problem than having a bad thumbnail, so don't run
        # mythtranscode and mythpreviewgen. Instead, use ffmpeg to generate the
        # thumbnail.

        # cmd = 'mythtranscode --buildindex --allkeys --infile "%s"' \
            #     % (episode.path)
            # rc = subprocess.call(cmd, shell=True)

        # cmd = 'mythpreviewgen --seconds %d --infile "%s" --outfile "%s" --size %dx%d' \
            #     % (delay, episode.path, episode.thumbnail_path, episode.width, episode.height)
            # rc = subprocess.call(cmd, shell=True)

        subprocess.call(["ffmpeg",
                         "-ss", str(delay),
                         "-i", episode.path,
                         "-y",
                         "-vframes", "1",
                         "-loglevel", "quiet",
                         episode.thumbnail_path])

        old_umask = os.umask(000)
        os.chmod(episode.thumbnail_path, stat.S_IWUSR | stat.S_IRUSR | stat.S_IWGRP | stat.S_IRGRP | stat.S_IWOTH | stat.S_IROTH)
        os.umask(old_umask)


    def download_thumbnail(self, episode):
        episode.thumbnail_extension = os.path.splitext(episode.thumbnail_url)[1].lower()
        episode.thumbnail_path = episode.path + episode.thumbnail_extension

        self.logger.info("downloading thumbnail...")
        self.logger.info("  remote URL: " + episode.thumbnail_url)
        self.logger.info("  local path: " + episode.thumbnail_path)

        urllib.urlretrieve(episode.thumbnail_url, episode.thumbnail_path)

        # If the downloaded thumbnail is not the right image type, convert it.
        if episode.thumbnail_extension != self.required_thumbnail_extension:
            self.logger.info("converting thumbnail image to " + self.required_thumbnail_extension)
            old_thumbnail_path = episode.thumbnail_path
            episode.thumbnail_extension = self.required_thumbnail_extension
            episode.thumbnail_path = episode.path + episode.thumbnail_extension
            Image.open(old_thumbnail_path).save(episode.thumbnail_path)
            os.remove(old_thumbnail_path)

        old_umask = os.umask(000)
        os.chmod(episode.thumbnail_path, stat.S_IWUSR | stat.S_IRUSR | stat.S_IWGRP | stat.S_IRGRP | stat.S_IWOTH | stat.S_IROTH)
        os.umask(old_umask)


#===============================================================================

if __name__ == "__main__":

    # Parse the command-line.

    opt_parser = ArgumentParser(description = "Download videos and store them in the MythTV database")
    opt_parser.add_argument("-c", "--conf-file",
                            default = "~/.mythtv/feedme.conf",
                            help = "location of configuration file (default: %(default)s)")
    opt_parser.add_argument("-m", "--mythtv-conf-file",
                            default = "~/.mythtv/config.xml",
                            help = "location of MythTV configuration file (default: %(default)s)")
    opt_parser.add_argument("-l", "--log-file",
                            help = "location of file to store log messages (default: None)")
    opt_parser.add_argument("-v", "--log-level",
                            help = "verbosity level of the log messages (debug|info|warning|error|critical) (default: info)")
    opt_parser.add_argument("-f", "--feed",
                            action = "append",
                            help = "feed name to process (may be included multiple times) (default: process all feeds)")
    opts = opt_parser.parse_args()


    # Set up the logger.

    logger = logging.getLogger("feedme")
    logger.setLevel(logging.INFO)
    if opts.log_level is not None:
        logger.setLevel(getattr(logging, opts.log_level.upper()))
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    if opts.log_file is None:
        handler = logging.StreamHandler()
    else:
        handler = logging.FileHandler(os.path.expanduser(opts.log_file), mode="w")
    handler.setFormatter(formatter)
    logger.addHandler(handler)


    # Read the config file.

    opts.conf_file = os.path.expanduser(opts.conf_file)
    logger.info("reading config file " + opts.conf_file)
    if not os.access(opts.conf_file, os.R_OK):
        logger.error("cannot read config file, aborting")
        sys.exit(-1)
    config = RawConfigParser()
    config.read(opts.conf_file)


    # Create the database handler.

    config_db = etree.parse(opts.mythtv_conf_file).find("Database")

    db = MythDB(
        host=config_db.find("Host").text.strip(),
        port=int(config_db.find("Port").text.strip()),
        dbname=config_db.find("DatabaseName").text.strip(),
        username=config_db.find("UserName").text.strip(),
        password=config_db.find("Password").text.strip(),
    )


    # Find the list of sections from the config file to process.

    sections = opts.feed
    if sections is None:
        sections = config.sections()

    # Start feedme.

    feedme = Feedme(config, db)
    feedme.run(sections)
