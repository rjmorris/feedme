import feedparser
import logging


class GrabberBase:
    feed = None
    episodes = None
    logger = None

    def __init__(self, url):
        self.logger = logging.getLogger("feedme")
        self.episodes = []

        self.parse_feed(url)
        if self.feed is None: return
        if not self.feed.has_key("entries"): return

        for entry in self.feed.entries:
            episode = self.parse_entry(entry)
            if episode is None:
                continue
            elif episode.is_valid():
                self.logger.debug("found episode: " + episode.title)
                self.episodes.append(episode)
            else:
                self.logger.warn("skipping episode with invalid data:")
                self.logger.warn("  title      = " + str(episode.title))
                self.logger.warn("  url        = " + str(episode.url))
                self.logger.warn("  start_date = " + str(episode.start_date))
                self.logger.warn("  mime_type  = " + str(episode.mime_type))

    def parse_feed(self, url):
        self.logger.info("parsing feed at URL " + url)
        self.feed = feedparser.parse(url)

    def parse_entry(self, entry):
        raise Exception("The parse_entry method must be overridden by the grabber class.")

    def get_episodes(self):
        return self.episodes
