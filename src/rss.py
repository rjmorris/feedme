from feedme import Episode
from grabberbase import GrabberBase
from datetime import datetime
import time


# Grabber for basic RSS feeds.

class RSS(GrabberBase):

    published_date = None

    def __init__(self, url):
        GrabberBase.__init__(self, url)
        self.published = datetime.now()

    def parse_entry(self, entry):
        episode = Episode()

        #------------------------------------------------------------------------
        # Get the episode title.

        if entry.has_key("title"):
            episode.title = entry.title

        #------------------------------------------------------------------------
        # Get the episode description.

        if entry.has_key("description"):
            episode.description = entry.description

        #------------------------------------------------------------------------
        # Get the episode's start date. The feedparser module recognizes many
        # date formats, but we've run across a few that it doesn't. Check for
        # those if the automatic detection didn't work. If we still can't parse
        # the date, then simply take the date from the previous episode, or
        # today's date if this is the first episode in the feed.

        if entry.has_key("published_parsed") and entry.published_parsed is not None:
            self.published_date = datetime.fromtimestamp(time.mktime(entry.published_parsed))
        elif entry.has_key("published") and len(entry.published) > 0:                
            try:
                self.published_date = datetime.strptime(entry.published, "%a %b %d %Y %H:%M:%S +0000")
            except(ValueError):
                try:
                    self.published_date = datetime.strptime(entry.published, "%a, %b %d %Y %H:%M:%S +0000")
                except(ValueError):
                    try:
                        self.published_date = datetime.strptime(entry.published, "%a, %d %b %Y %H:%M:%S +0000")
                    except(ValueError):
                        # Give up. Don't alter self.published_date, so it keeps
                        # the value from the previous episode.
                        pass
        else:
            # Give up. Don't alter self.published_date, so it keeps
            # the value from the previous episode.
            pass

        episode.start_date = self.published_date

        #------------------------------------------------------------------------
        # Get the episode's video URL. There may be more than one. If so, use
        # the one whose video has the largest file size, or the first one if
        # file size information isn't available.

        if entry.has_key("enclosures") \
                and len(entry.enclosures) > 0:
            longest_enclosure = None
            max_enclosure_length = 0
            for enclosure in entry.enclosures:
                if enclosure.has_key("length") and int(enclosure.length) > max_enclosure_length:
                    longest_enclosure = enclosure
                    max_enclosure_length = int(enclosure.length)

            if longest_enclosure is None:
                longest_enclosure = entry.enclosures[0]

            episode.url = longest_enclosure.get("url")
            episode.mime_type = longest_enclosure.get("type")

        #------------------------------------------------------------------------
        # Get the episode's thumbnail URL. This attribute isn't supported in
        # basic RSS feeds, so set it to None.

        episode.thumbnail_url = None


        return episode
