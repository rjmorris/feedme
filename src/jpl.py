from itunes import iTunes
import os.path


# Grabber for the NASA JPL feed. The feed has an error in which all files have a
# mime type of audio/mpeg, even though they are videos. This grabber changes the
# mime type.

class JPL(iTunes):

    def parse_entry(self, entry):
        episode = iTunes.parse_entry(self, entry)

        if episode.mime_type == "audio/mpeg" and os.path.splitext(episode.url)[1] == ".m4v":
            episode.mime_type = "video/x-m4v"

        return episode
