from rss import RSS


# Grabber for feeds implementing the iTunes RSS extension (namespace
# xmlns:itunes).

class iTunes(RSS):

    def parse_entry(self, entry):
        episode = RSS.parse_entry(self, entry)

        # The iTunes extension defines a subtitle field that is required to be
        # plain text and 255 characters or fewer. This is likely better than the
        # description field, which may contain HTML markup and be very long.
        if entry.has_key("subtitle"):
            episode.description = entry.subtitle

        # The iTunes extension defines an optional thumbnail attribute.
        if entry.has_key("itunes_image") \
                and len(entry.itunes_image) > 0:
            episode.thumbnail_url = entry.itunes_image[0].get("url")

        return episode
