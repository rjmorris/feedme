Feedme is a Python script for downloading video podcasts and storing them with your MythTV recordings.

# How it works

1. Create a config file listing the podcasts you want Feedme to download. You provide each podcast's feed URL and a few options, such as the maximum number of episodes to download.
1. Set up Feedme to run periodically.
1. When Feedme runs, it checks your podcasts for new episodes, downloads the videos, and inserts them into the MythTV database so they appear just like any other recording.

# Installation and setup

Although not required, you'll most likely install Feedme on the same machine that runs your MythTV backend service. It is known to be compatible with MythTV 0.29.

In addition to MythTV, the following external dependencies must be installed before running Feedme:

- Python 3.6+
- Python packages:
    - feedparser
    - lxml
    - PIL
    - sqlalchemy
- ffmpeg (for creating thumbnail screenshots of the videos)
- mediainfo (for obtaining technical details of the videos)

Feedme adds the downloaded videos to MythTV's list of recordings, but MythTV requires recordings to come from a channel. I recommend creating a dedicated "Feedme" channel, which you can do through `mythtv-setup`. Go to the channel editor and add a new channel. Enter a channel name and number, and leave everything else at their default values. The recommended values are "Feedme" for the name and 9998 for the number.

To install Feedme, copy the contents of [src/](src) to a directory of your choice.

# Configuration

An example configuration file is provided in [feedme.conf](feedme.conf). Copy it somewhere (such as `$HOME/.mythtv/feedme.conf` and edit it to your liking. It's an INI-format file with the following sections:

- `[feedme]` - General settings for the Feedme application.
- `[defaults]` - Default settings for each podcast.
- `[<podcast name>]` - Settings for a particular podcast. Many of these may appear in the file. (Replace `<podcast name>` with whatever name you choose to call the podcast.)

## General settings

These settings go in the `[feedme]` section of the configuration file. The available settings are:

- `channel_name` - Name you gave to the Feedme channel when setting it up in `mythtv-setup`. Default: "Feedme".
- `channel_num` - Number you gave to the Feedme channel when setting it up in `mythtv-setup`. Default: 9998.
- `channel_id` - Database ID assigned to the Feedme channel. You'll probably need to inspect the database's `channel` table to find this value. Default: 9998.
- `videos_dir` - Directory holding your MythTV recordings. Note that the name "videos" is misleading here. It doesn't refer to where your MythTV videos are stored. Default: "/data/mythtv/recordings".
- `title_len` - Maximum number of characters allowed for a podcast title. Titles longer than this will be truncated. Default: 40
- `subtitle_len` - Maximum number of characters allowed for a podcast episode's title. Episode titles longer than this will be truncated. Default: 128

## Podcast settings

The following settings are required in each podcast-specific section of the configuration file:

- `url` - URL where the podcast's feed can be accessed.
- `grabber` - Name of the Feedme "grabber" plugin to use for parsing the podcast's feed. Feedme comes with several grabbers, of which `itunes` and `mediarss` are most likely to work with a variety of feeds, so try one of those first. It also comes with several podcast-specific grabbers that have been customized to work with that podcast's feed. These are unlikely to work with feeds other than the ones they were designed for, but they may provide examples of writing additional grabbers.

The following settings can go in the `[default]` section of the configuration file or in a podcast-specific section. Values in a podcast-specific section override values in the default section. The available settings are:

- `earliest_date` - Don't download episodes that were released earlier than this date. Formatted as YYYY-MM-DD. Default: 1900-01-01
- `download_newest` - When more episodes are available than will be downloaded, download the newest episodes instead of the oldest episodes. Default: true
- `max_unwatched` - Don't download episodes if you already have more than this many unwatched episodes of this podcast in your MythTV database. Default: 10
- `download_thumbnail` - If the podcast provides thumbnail images for its episodes, use them. Otherwise, create them by taking a screenshot approximately 1 minute into the video. Default: true
- `category` - Category to assign to episodes. You can filter recordings by category in the MythTV frontend. Examples might be Tech, Science, or Entertainment. Default: "Feeds"

# Running the script

Feedme must have access to your MythTV database. Therefore, you should run it as a user with access to the database, the easiest being the same user who runs the MythTV backend service (typically user `mythtv`). For database connection details, you'll tell Feedme the location of your MythTV backend configuration file (typically located at `$HOME/.mythtv/config.xml` or `$MYTHCONFDIR/config.xml`).

An example invocation is:

```
sudo -u mythtv python3 feedme.py \
    --conf-file=$HOME/.mythtv/feedme.conf \
    --log-file=$HOME/.mythtv/feedme.log \
    --log-level=debug \
    &> $HOME/.mythtv/feedme.err
```

You can also pass the `--feed` option to limit the operation to specific podcasts defined in your config file. For example, if you have a `TED Talks` section in your config file, you can add `--feed "TED Talks"` to update your TED episodes without processing all your other podcasts.

Optional, but recommended: Create a cron job for running Feedme regularly.

# Limitations

- Feedme doesn't support downloading videos from YouTube channels.
- Feedme doesn't support audio podcasts. Videos only.
- If mythpreviewgen attempts to regenerate a thumbnail, the image it produces is
sometimes garbled. The initial thumbnail generated or downloaded by Feedme
appears fine. One circumstance in which mythpreviewgen regenerates the thumbnail
is when you exit a video in the middle without deleting it.
